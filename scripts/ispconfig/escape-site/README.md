# Escape site

A site created as "default server" site.

## The problem

Sometimes there is a problem with a specific site's configuration in Apache, e.g. https problems like Let's Encrypt misconfiguration, etc. In situations like this Apache decides to serve a "random" site (usually the most recently added) in place of the site asked originally. We can see which of our sites is the "default" site for the server using the command: `apache2ctl -S | grep "default server"`. This site is presented whenever someone asks Apache about a site that isn’t defined properly in its configuration. A common example is a site that has no SSL configuration and yet is requested as https.
This of course is very bad, because the visitor is presented with a totally different site under the name of the site requested!

## Use case

The (quick’n’dirty) solution below is useful when we want a simple “landing” page to catch all domain requests to our Apache server that cannot be resolved. For security and stability reasons, we may want to configure the server to completely deny such requests.

## A solution

In order to overcome the above situation, we could adjust the web server in such a way that the server always uses a specific site as a fallback when something in another site's configuration goes wrong. That site doesn't have to be a real one, e.g. no DNS records or domain possession needed. It’s just a dummy site that is presented in case Apache doesn’t know what to serve.
The trick is based in [previous work](https://gitlab.com/kkatsaros/catch-all-site/-/tree/master/).

## Script actions

The install script creates the escape site, namely “escape.site”. This site shouldn’t appear in ISPConfig sites list, so it is configured independently and a (self-signed) SSL certificate is issued for it. Then the vhost file `000-default.conf` is overwritten with a proper one and web server is reloaded.

## Testing

We may check the outcome with the command `apache2ctl -S | grep "default server"`. It should have an output like this:
```
# apache2ctl -S | grep "default server"
         default server escape.site (/etc/apache2/sites-enabled/000-default.conf:1)
         default server escape.site (/etc/apache2/sites-enabled/000-default.conf:19)
```
and from now on Apache should respond with the newly created site to any requests for sites that, for various reasons, cannot be served.

To test the actual behavior, we may add a made-up domain name together with our server IP to our system’s (e.g. our laptop’s, **not the server’s**) host file (e.g. `/etc/hosts`):
```
XXX.XXX.XXX.XXX a-wrong-site.com
```
Then we point our browser to “http://a-wrong-site.com/”. We should see our “site not found” page under the address.
![](a-wrong-site.com.png)
