#!/bin/bash

echo "Setting up an Escape site..."
mkdir -p /var/log/ispconfig/httpd/escape.site /var/www/escape.site/ssl
mv /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/000-default.conf.$(date +%Y%m%d)
cp 000-default.conf /etc/apache2/sites-enabled/000-default.conf
cp -R web /var/www/escape.site/web
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout /var/www/escape.site/ssl/escape.site.key -out /var/www/escape.site/ssl/escape.site.crt -subj "/CN=escape.site"
service apache2 reload
