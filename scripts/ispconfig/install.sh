#!/bin/bash

M="\033[0;34m" # message, blue
Q="\033[0;36m" # question, light cyan
W="\033[0;31m" # warning, red
N="\033[0m"    # no color

mkdir -p /tmp/dosuite
cd /tmp/dosuite

# Check whether ISPConfig is installed. If not, offer to install.
echo -e "${M}Checking for ISPConfig installation...${Q}"
if ! which ispconfig_update.sh > /dev/null; then
  read -r -p "ISPConfig not found. Do you want to install it? (y to install, any other key to cancel): " ispconfig
  if [[ "$ispconfig" == "y" ]]; then
    echo -e "${W}Please, be aware that the official automated ISPConfig installation script has to be executed only in fresh Linux systems.${N}"
    echo -e "${W}Installing in a working system may cause severe problems to established programs and services.${Q}"
    read -r -p "Are you sure you want to install ISPConfig on this system (`hostname -f`)? (y to install, any other key to cancel): " sure
    if [[ "$sure" == "y" ]]; then
      echo -e "$N"
      wget https://git.ispconfig.org/ispconfig/ispconfig-autoinstaller/-/raw/master/ispc3-ai.sh
      echo -e "${Q}You have the option to install a web-only server or a complete ISP server (web, mail, DNS, firewall etc)."
      read -r -p "Do you want to install a FULL ISPConfig server? If you press "y", you will end up with a full server, any other key or Enter for web-only server." fullserver
      if [[ "$fullserver" == "y" ]]; then
        echo -e "${M}Performing a full ISPConfig installation, this may take some time...${N}"
        sh ispc3-ai.sh --use-php=7.4,8.0,8.1 --i-know-what-i-am-doing
      else
        echo -e "${M}Installing ISPConfig for web server only, this may take some time...${N}"
        sh ispc3-ai.sh --use-php=7.4,8.0,8.1 --no-mail --no-dns --use-unbound --no-firewall --i-know-what-i-am-doing
      fi
      echo -e "${W}Please, take a note of the ISPConfig admin password and MariaDB root password.${N}"
      read -r -p " Press any key to continue... " nothing
    else
      echo -e "${M}Exiting...${N}"
      exit
    fi
  else
    echo -e "${M}Exiting...${N}"
    exit
  fi
fi
echo -e "${M}...done.${N}"

# Check whether git, bindfs and wp-cli are installed. If not, install.
echo -e "${M}Checking if git is installed...${N}"
if ! which git > /dev/null; then
  echo -e -e "${M}git not found. Installing...${N}"
  apt install -y git
fi
echo -e "${M}...done.${N}"

echo -e "${M}Checking if bindfs is installed...${N}"
if ! which bindfs > /dev/null; then
  echo -e -e "${M}bindfs not found. Installing...${N}"
  apt install -y bindfs
fi
echo -e "${M}...done.${N}"

echo -e "${M}Checking if wp-cli is installed...${N}"
if ! which wp > /dev/null; then
  echo -e -e "${M}wp-cli not found. Installing...${N}"
  wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  chmod +x wp-cli.phar
  mv wp-cli.phar /usr/local/bin/wp
fi
echo -e "${M}...done.${N}"

exit
