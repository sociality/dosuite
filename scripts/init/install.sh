#!/bin/bash

# Function for replacing lines in files
changetext()
{
  OLDTEXT=$1
  NEWTEXT=$2
  TARGETFILE=$3
  LN=`grep -n -m 1 "$OLDTEXT" $TARGETFILE | cut -d: -f1`
  sed -i "$LN d" $TARGETFILE
  sed -i "$LN i $NEWTEXT" $TARGETFILE
}

echo "This script will apply some initial additions and configurations in your server."
echo "The script must be executed in a clean Debian system. Running repeatedly or in already established systems may mess things, watch your steps."
echo "If you don't want to continue, please press Ctrl+C."
echo
read -r -p "Please, give an email address as a RECIPIENT for notifications from this server: " MAILTO
echo "For sending emails, a real email account is needed. Please, input your SMTP credential for this to work."
read -r -p "SMTP host (i.e. mail.example.com): " SMTPHOST
read -r -p "SMTP port (i.e. 587): " SMTPPORT
read -r -p "SMTP user (i.e. user@example.com): " SMTPUSER
read -r -p "SMTP (email) password: " SMTPPASS
read -r -p "STARTTLS (on: STARTTLS | off: SSL): " STARTTLS

# Set correct timezone
timedatectl set-timezone Europe/Athens

BCKPDATE=$(date +%Y%m%d)

# Create useful files/directories
mkdir -p /root/scripts /root/.ssh /root/.secrets /root/.secrets/keys
touch /root/.ssh/authorized_keys /root/.ssh/config

# Additional configuration
echo "Creating backup of file /etc/bash.bashrc."
cp /etc/bash.bashrc /etc/bash.bashrc.$BCKPDATE
cat >> /etc/bash.bashrc <<'_EOF'
# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
_EOF
echo "Creating backup of file /root/.bashrc."
cp /root/.bashrc /root/.bashrc.$BCKPDATE
cat > /root/.bashrc <<'_EOF'
color_prompt=yes
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@$(hostname -f)\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\] \$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@$(hostname -f):\w \$ '
fi
unset color_prompt force_color_prompt
export LS_OPTIONS='--color=auto --group-directories-first'
eval "`dircolors`"
alias ll='ls $LS_OPTIONS -lah'
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"
_EOF
source ~/.bashrc
cat > /root/scripts/update <<'_EOF'
#!/bin/bash
apt update
apt dist-upgrade
apt autoremove
apt autoclean
_EOF
chmod -R 700 /root/scripts /root/.ssh /root/.secrets

# Fix locale
echo "Creating backup of file /etc/environment."
cp /etc/environment /etc/environment.$BCKPDATE
cat >> /etc/environment <<'_EOF'
LANG=en_US.UTF-8
LANGUAGE=en_US
LC_CTYPE=en_US.UTF-8
LC_NUMERIC=en_US.UTF-8
LC_TIME=en_US.UTF-8
LC_COLLATE=en_US.UTF-8
LC_MONETARY=en_US.UTF-8
LC_MESSAGES=en_US.UTF-8
LC_PAPER=en_US.UTF-8
LC_NAME=en_US.UTF-8
LC_ADDRESS=en_US.UTF-8
LC_TELEPHONE=en_US.UTF-8
LC_MEASUREMENT=en_US.UTF-8
LC_IDENTIFICATION=en_US.UTF-8
_EOF

# Install needed packages
apt update
apt -y install mc htop net-tools unattended-upgrades apt-listchanges screen msmtp
ln -s /usr/bin/msmtp /usr/sbin/sendmail

# Configure email sending (restricted to root)
cat << EOF > /root/.msmtprc
account        mailaccount
host           $SMTPHOST
port           $SMTPPORT
from           $SMTPUSER
user           $SMTPUSER
password       $SMTPPASS
auth           on
tls            on
tls_starttls   $STARTTLS
tls_certcheck  on
logfile        /var/log/msmtp.log
account default : mailaccount
EOF

# Send a test mail
echo "Subject: New server configuration - `hostname -f`" > /tmp/mail.txt
echo "Your new server at `hostname -f` was just configured. Enjoy!" >> /tmp/mail.txt
sendmail $MAILTO < /tmp/mail.txt
rm /tmp/mail.txt

# Configure and execute Unattended Upgrades
cat > /etc/apt/apt.conf.d/20auto-upgrades <<'_EOF'
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::AutocleanInterval "7";
_EOF

# Choose random time to upgrade
TIMEPOOL=("01:00" "01:30" "02:00" "02:30" "03:00" "03:30" "04:00" "04:30" "05:00" "05:30" "06:00")
REBOOTTIME=${TIMEPOOL[ $RANDOM % ${#TIMEPOOL[@]} ]}

cat << _EOF > /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::Origins-Pattern {
"site=*";
};
Unattended-Upgrade::Mail "$MAILTO";
Unattended-Upgrade::MailReport "on-change";
Unattended-Upgrade::Sender "$SMTPUSER";
Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";
Unattended-Upgrade::Remove-New-Unused-Dependencies "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Automatic-Reboot "true";
Unattended-Upgrade::Automatic-Reboot-WithUsers "true";
Unattended-Upgrade::Automatic-Reboot-Time "$REBOOTTIME";
_EOF

service unattended-upgrades restart
unattended-upgrades -d

# Decrease grub menu timeout.
changetext "GRUB_TIMEOUT=5" "GRUB_TIMEOUT=2" /etc/default/grub
update-grub

echo "Outgoing mail configured. There was an attempt to send a test email to $MAILTO. Please, check your inbox."
read -r -p "The system will now reboot, please press ENTER." NOTHING
reboot
