#!/bin/bash

apt update
apt -y install git
git clone https://gitlab.com/sociality/dosuite.git /tmp/dosuite
cd /tmp/dosuite/scripts
bash init/install.sh
bash ispconfig/install.sh
bash ispconfig/escape-site/install.sh
