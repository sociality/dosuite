# DOsuite

**WORK IN PROGRESS, NOT USABLE AT THE MOMENT.**

A DevOps suite on Linux shell, tailored for multisite operations inside a shared hosting environment. Focused on [Wordpress](https://wordpress.org/) with the use of [ISPConfig](https://www.ispconfig.org/).

### POC
The project assumes that there is a need for a Linux shell (SSH/SFTP) user to have read-write access to all of the web-roots in a web server in order to execute DevOps. To avoid file permission problems, the web-roots (and the rest of the site structure) are individualy remounted to the user's workspace with proper permissions. This achieves the desired multi-site editing, keeping the sites isolated in the web server level for security.

### DISCLAIMER
This style of working in shared hosting environment may affect many assets (sites) at the same time. Thus, although there is no root access needed during usage, the available operations are still **DANGEROUS**.
**Use this at your own risk!**

### Prerequisites
- Linux, server-side (tested on Debian 10-11).
- Root access for the installation.

### System customization Features
During installation the system is altered in order to form a proposal for a DevOps environment. One may not like some of these customizations. You may alter any of them to fit your taste.

The script will take care of all things to be installed and configured. It will check and resolve whether the following are present in system:
- Git.
- ISPConfig for web server management. The official installation script is located [here](https://git.ispconfig.org/ispconfig/ispconfig-autoinstaller) (Debian/Ubuntu). **The suite is not tested in any other environment (plain web server, Plesk, CPanel etc).**
- [`bindfs`](https://bindfs.org/) package in order to remount sites with specific user permissions.
- [`wp-cli`](https://wp-cli.org/) for in-site actions.
- A shell user dedicated for this purpose.

### DevOps Features
The automation affects the following special functionality:
1.  Backup the current Wordpress site (files and database). Sensitive information such as web user, database user and password, etc are stripped from the backup file. The derived .tar.gz file tries to keep up with the CPanel backup file structure.
2.  Restore site (not yet implemented).
3.  Create a site in current web-root. Choice to install and even activate predefined plugins.
4.  Fetch a site from a remote (or local) server to current web-root.

The fetch-site feature only works if ssh passwordless authentication is implemented from the local to the source (remote) server. Aliased connections using `~/.ssh/config` are a must too. The implementation of these is not covered by this suite and is not in the scope of this documentation.

### Installation
Please, make sure that the FQDN of the server is setup correctly and DNS propagation has completed before starting the installation procedure. You may check propagation stage using [this tool](https://dnschecker.org/). Once you are sure that Let's Encrypt's servers can find your FQDN at the given IP, just execute the following command as root on the fresh Debian stable system:

`# wget -O /tmp/begin.sh https://gitlab.com/sociality/dosuite/-/raw/master/prepare/begin.sh && bash /tmp/begin.sh`

The script will do the rest according to your choices.

### Configuration


### Usage
